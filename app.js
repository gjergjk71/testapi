const graphqlServer = require("graphql-yoga").GraphQLServer;
const { Prisma } = require("prisma-binding");
const prismaTypeDefs = require("./generated/prisma-client/prisma-schema.js").typeDefs;
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const configs = {
	jwt_secret:"secret_key"
}

const saltRounds = 10;

const createToken = (userId) => {
	const token = jwt.sign({
		userId: userId
	},configs.jwt_secret);
	return token
}


const prismaDb = new Prisma({
	typeDefs:prismaTypeDefs,
	endpoint:"https://eu1.prisma.sh/gjergj-kadriu-c6f550/testapi/dev",
	debug: false
})


const login = async (root,args,context) => {
	if (!args.username || !args.password){
		throw new Error("Please check that all of your arguments are not empty!")
	}
	const user = await prismaDb.query.user({where:{username:args.username}});
	if (!user){
		throw new Error("Invalid credentials");
	}
	const validPassword = await bcrypt.compare(args.password,user.password);
	if (!validPassword){
		throw new Error("Invalid credentials");	
	}
	return {
		userId: user.id,
		token: createToken(user.id),
		expiresIn: 1
	};
}

const signUp = async (root,args,context) => {
	if (!args.username || !args.first_name || !args.last_name || !args.password){
		throw new Error("Please check that all of your arguments are not empty!")
	}
	const hashed_password = await bcrypt.hash(args.password,saltRounds);
	var userParams = {
		first_name: args.first_name,
		last_name: args.last_name,
		username: args.username,
		password: hashed_password
	}
	const user = await prismaDb.mutation.createUser({data:userParams});
	return {
		userId: user.id,
		token: createToken(user.id),
		expiresIn: 1
	};
}

const server = new graphqlServer({
	typeDefs: "./schema.graphql", 
	resolvers:{
		Query: {
			categories: async (parent,args,context) => {
				return await prismaDb.query.categories();
			},
			login: login
		},
		Mutation: {
			createCategory: async (parent,args) => {
				if (!args.name){
					throw new Error("name arg must not be empty");
				}
				return await prismaDb.mutation.createCategory({
					data:{
						name: args.name
					}
				})
			},
			signUp: signUp
		}
	},
	context: req => {
		console.log(req.request.headers["authorization"]);
	}
});

server.start(() => console.log("Running on port 4000"));
